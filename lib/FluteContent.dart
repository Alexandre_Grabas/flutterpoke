import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' as convert;
import 'api/Tweet.dart';

class FluteButton extends StatelessWidget{

  String img;

  FluteButton(this.img);

  @override
  Widget build(BuildContext context) {
    return IconButton(onPressed: (){},icon : Image.asset(this.img),
    );
  }
}

class FluteContent extends StatefulWidget{

  @override
  State<FluteContent> createState() => _FluteContentState();
}

class _FluteContentState extends State<FluteContent> {



  @override
  Widget build(BuildContext context) {

    Tweet? tweet;

    void callApi() async {
      //Entrée api Rest
      var response = await http.get(Uri.parse(
          "https://raw.githubusercontent.com/Chocolaterie/EniWebService/main/api/tweets.json"));

      //http to json
      var json = convert.jsonDecode(response.body);

      //json to Movie
      tweet = Tweet.jsonToTweet(json);

      //Rafraichissement de la vue
      setState(() {});

      //Print
      // print(
      //     "id : ${movie.id} titre :  ${movie.title} truc :  ${movie.description}");
    }

    return Container(
        child: Column(
          children: [
            Row(crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Image(
                  image: AssetImage('assets/images/chienFlute.jpg'),
                  width: 200,
                ),
                Expanded(
                  child: Container(
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Row(
                                children: [
                                  Expanded(
                                      child: Text(
                                        "Jean@michel.oignon",
                                        textAlign: TextAlign.start,
                                      )),
                                  Expanded(
                                      child: Text(
                                        "21",
                                        textAlign: TextAlign.end,
                                      )),
                                ],
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Text(
                                "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce ac justo vel augue dictum bibendum et vel tortor. Sed vestibulum, massa non tristique sollicitudin, justo dui facilisis juste, ut tristique eros lectus ut urna. Présent dans l'hendrerit gratuit. Integer tristique dit-on risus pharetra, non vestibulum purus sagittis. Pas de nécéssté. Sed consequat sapien id juste venenatis, non dictum juste lacinia. Vivamus au fait que id élite bibendum malesuada.",
                                textAlign: TextAlign.start,
                              ),
                            ),
                          ])),
                ),
              ],
            ),
            Container(
              color: Colors.grey,
              child: Padding(
                padding: const EdgeInsets.fromLTRB(0, 7, 0, 7),
                child: Row(
                  children: [
                    Expanded(child: FluteButton("images/reply.png")),
                    Expanded(
                        child: FluteButton("images/retweet.png")),
                    Expanded(
                      child: FluteButton("images/favorite.png"),)
                  ],
                ),
              ),
            )
          ],
        ));
  }
}