import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' as convert;
import 'api/Tweet.dart';

class FluteButton extends StatefulWidget {
  String img;

  FluteButton(this.img);

  @override
  State<FluteButton> createState() => _FluteButtonState();
}

class _FluteButtonState extends State<FluteButton> {
  @override
  Widget build(BuildContext context) {
    return IconButton(
      onPressed: () {},
      icon: Image.asset(this.widget.img),
    );
  }
}

class FluteContentApi extends StatefulWidget {
  @override
  State<FluteContentApi> createState() => _FluteContentState();
}

class _FluteContentState extends State<FluteContentApi> {

  List<Tweet> tweets = [];

  void callApi() async {
    //Entrée api Rest
    var response = await http.get(Uri.parse(
        "https://raw.githubusercontent.com/Chocolaterie/EniWebService/main/api/tweets.json"));

    //http to json
    var json = convert.jsonDecode(response.body);

    //json to Movie
    tweets = List<Tweet>.from(
        json.map((tweetJson) => Tweet.jsonToTweet(tweetJson)));

    //Rafraichissement de la vue
    setState(() {});

    //Print
    // print(
    //     "id : ${movie.id} titre :  ${movie.title} truc :  ${movie.description}");
  }

  @override
  Widget build(BuildContext context) {
    callApi();

    return Column(children: [
      // OutlinedButton(
      // onPressed: () {
      //   callApi();
      // },
      // child: Text("Le BOUTON !!!")),
      Expanded(
    child: ListView.builder(
        itemCount: tweets.length,
        itemBuilder: (BuildContext context, int index) {
          // Faire la vue d'une cellule
          return Container(
              child: Column(
            children: [
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Image(
                    image: AssetImage('assets/images/chienFlute.jpg'),
                    width: 200,
                  ),
                  Expanded(
                    child: Container(
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Row(
                              children: [
                                Expanded(
                                    child: Text(
                                  "${tweets[index].author}",
                                  textAlign: TextAlign.start,
                                )),
                                Expanded(
                                    child: Text(
                                  "${tweets[index].id}",
                                  textAlign: TextAlign.end,
                                )),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(
                              "${tweets[index].message}",
                              textAlign: TextAlign.start,
                            ),
                          ),
                        ])),
                  ),
                ],
              ),
              Container(
                color: Colors.grey,
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(0, 7, 0, 7),
                  child: Row(
                    children: [
                      Expanded(child: FluteButton("images/reply.png")),
                      Expanded(child: FluteButton("images/retweet.png")),
                      Expanded(
                        child: FluteButton("images/favorite.png"),
                      )
                    ],
                  ),
                ),
              )
            ],
          ));
        }),
      )
    ]);
  }
}
