import 'package:flutter/material.dart';

class FooterButton extends StatelessWidget {
  String label;

  FooterButton(this.label);

  @override
  Widget build(BuildContext context) {
    return Expanded(
        child: Text(
      this.label,
      textAlign: TextAlign.center,
    ));
  }
}

class FooterComponent extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.black12,
      child: Padding(
        padding: const EdgeInsets.fromLTRB(0, 20, 0, 20),
        child: Row(
          children: [
            Expanded(
                child: Text(
              "Fill",
              textAlign: TextAlign.center,
            )),
            Expanded(
                child: Text(
              "Notification",
              textAlign: TextAlign.center,
            )),
            Expanded(
                child: Text(
              "Message",
              textAlign: TextAlign.center,
            )),
            Expanded(
                child: Text(
              "Moi",
              textAlign: TextAlign.center,
            ))
          ],
        ),
      ),
    );
  }
}
