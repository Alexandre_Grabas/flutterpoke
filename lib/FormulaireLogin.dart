import 'package:flutter/material.dart';

class FormulaireLogin extends StatelessWidget {
  var formKey=GlobalKey<FormState>();

  bool switchState=false;

  void onSubmit(BuildContext context){
    if (formKey.currentState!.validate()){
    print("validateur");
    Navigator.pushNamed(context, "/Home");
    }
  }

  String? validIdentifiant(String? value){
    if (value!.length<2){
      return "blebleble pas bien";
    }
    return null;
}

  String? validMDP(String? value){
    if (value!.length<3){
      return "Mot de passe trop nul";
    }
    return null;
  }

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Form(key: formKey,
            child: Column(children: [
          TextFormField(validator: validIdentifiant,decoration: InputDecoration(labelText: "Identifiant")),
          TextFormField(validator: validMDP ,decoration: InputDecoration(labelText: "Mot de passe")),
          Row(children: [
            Switch(value: switchState, onChanged: (value){switchState=value;}),
            Text("Mémoriser mes informations")
          ],
          ),
          Container(color:Colors.blue,
            child: Row(
              children: [
                Expanded(child: OutlinedButton(onPressed:(){ onSubmit(context);}, child: Padding(
                  padding: const EdgeInsets.all(13.0),
                  child: Text("Connexion",style: TextStyle(color:Colors.black),),
                ))),
              ],
            ),
          )

        ],)),
      ),
    );
  }
}