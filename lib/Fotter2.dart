import 'package:flutter/material.dart';

class FooterButton extends StatelessWidget {
  String label;
  bool isActive;

  FooterButton(this.label, {this.isActive = false});

  Color getStateColor() {
    return this.isActive ? Color(0xff58B0F0) : Colors.black;
  }

  @override
  Widget build(BuildContext context) {
    return Expanded(child: Text(this.label, textAlign: TextAlign.center,style: TextStyle(color: getStateColor()),));
  }
}

class FooterComponent2 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.black12,
      child: Padding(
        padding: const EdgeInsets.fromLTRB(0, 20, 0, 20),
        child: Row(
          children: [
            FooterButton("Répondre", isActive: false,),
            FooterButton("Retweet",isActive: false,),
            FooterButton("Favoris",isActive: true,),
            FooterButton("Fill",isActive: true,),
          ],
        ),
      ),
    );
  }
}
