class Personne{
  int id;
  String nom;
  String prenom;
  int age;
  String race;
  List<String> passions;
  String sexe;
  String metier;

  Personne(this.id,this.nom,this.prenom,this.age,this.race,this.passions,this.sexe,this.metier);

  /**
   * Fonction de conversion de donnée json en objet Movie
   */
  static Personne jsonToPersonne(Map<String, dynamic> json){
    return Personne(json['id'],
                    json["nom"],
                    json["prenom"],
                    json["age"],
                    json["race"],
                    List<String>.from(json["passions"]),
                    json["sexe"],
                    json["metier"],
    );
  }
}