import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' as convert;
import 'Passion/Personne.dart';

class PassionApi extends StatefulWidget{
  @override
  State<PassionApi> createState() => _PassionApiState();
}

class _PassionApiState extends State<PassionApi> {

  List<Personne> personnes = [];


  void callApi() async {

    var response = await http.get(Uri.parse("http://localhost:8080/caracter/all"));



    //http to json
    var json = convert.jsonDecode(convert.Utf8Codec().decode(response.bodyBytes));

    //json to Movie
    personnes = List<Personne>.from(
        json.map((PersonneJson) => Personne.jsonToPersonne(PersonneJson)));

    //Rafraichissement de la vue
    setState(() {});

  }

  @override
  Widget build(BuildContext context) {

    callApi();

    return Container(
      child: Column(children: [
        Expanded(
          child: ListView.builder(itemCount: personnes.length,
          itemBuilder: (BuildContext context,int index){
            return Column(children: [
              Row(
                children: [
                  Text("${personnes[index].id}"),
                  Text("${personnes[index].nom}",style: TextStyle(fontFamily: 'Droid Sans')),
                  Text("${personnes[index].prenom}"),
                  Text("${personnes[index].age}"),
                  Text("${personnes[index].race}"),
                  RichText(
                  text: TextSpan(
                  text: "${personnes[index].passions}")),
                  Text("${personnes[index].passions}"),
                  Text("${personnes[index].sexe}"),
                  Text("${personnes[index].metier}"),


                ],
              )
            ],);
          }),
        )
      ],),
    );
  }
}
