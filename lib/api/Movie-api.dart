import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' as convert;

import 'movie.dart';

void main() {
  runApp(MovieApi());
}

class MovieApi extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Les film de l'infinit",
      home: MovieApiPage(),
    );
  }
}

class MovieApiPage extends StatefulWidget {

  @override
  State<MovieApiPage> createState() => _MovieApiPageState();
}

class _MovieApiPageState extends State<MovieApiPage> {
  Movie? movie;

  void callApi() async {
    //Entrée api Rest
    var response = await http.get(Uri.parse(
        "https://raw.githubusercontent.com/Chocolaterie/EniWebService/main/api/movie.json"));

    //http to json
    var json = convert.jsonDecode(response.body);

    //json to Movie
    movie = Movie.jsonToMovie(json);

    //Rafraichissement de la vue
    setState(() {});

    //Print
    // print(
    //     "id : ${movie.id} titre :  ${movie.title} truc :  ${movie.description}");
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Les film de l'infinit"),
      ),
      body: Container(
        child: Column(
          children: [
            Text("film : ${movie?.title} = ${movie?.description}"),
            OutlinedButton(onPressed: (){callApi();}, child: Text("sfddddddddddddddd"))
          ],
        ),
      ),
    );
  }
}
