class Movie{
  int id;
  String title;
  String description;

  Movie(this.id,this.title,this.description);


  /**
   * Fonction de conversion de donnée json en objet Movie
   */
  static Movie jsonToMovie(Map<String, dynamic> json){
    return Movie(json['id'], json["title"], json["description"]);
  }
}