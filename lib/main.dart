import 'package:flutter/material.dart';
import 'package:twitter_flute/FluteContent.dart';
import 'package:twitter_flute/FluteContentApi.dart';
import 'package:twitter_flute/FormulaireLogin.dart';
import 'package:twitter_flute/Fotter2.dart';
import 'package:twitter_flute/Header.dart';
import 'package:twitter_flute/PassionApi.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Twitter FLUTE',
      initialRoute: '/',
      routes: {
        '/': (context) => Connexion(),
        '/Home': (context) => HomePage(),
        '/Deux': (context) =>PageDeux(),
      },
      //home: HomePage(),
    );
  }
}

class HomePage extends StatelessWidget {

  void changePageHome(BuildContext context){
    Navigator.pushNamed(context, "/Home");
  }

   void changePageDeux(BuildContext context){
    Navigator.pushNamed(context, "/Deux");
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Flutter"),
      ),
      body: Column(
        children: [
          HeaderComponent(),
          //Expanded(child: SingleChildScrollView(child: IntrinsicHeight(child: FluteContent()))),
          Expanded(child: FluteContentApi()),
          OutlinedButton(onPressed:(){ changePageDeux(context);}, child: Text("PAGE 2")),
          FooterComponent2(),
        ],
      ),
    );
  }
}

class Connexion extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(appBar: AppBar(
      title: Text("CONNEXION FLULTIME"),
    ),
        body: Flex(direction: Axis.vertical,
            children: [
              FormulaireLogin(),
            ]));
  }
}

class PageDeux extends StatelessWidget{
  @override
  Widget build(BuildContext context){
    return Scaffold(      appBar: AppBar(
      title: Text("Flutter DEUX le retour du Flutter"),
    ),
    body: Flex(direction: Axis.vertical,
    children: [
      Text("L'Ultime pag ultime de l'ulitmation ultime ... ULTIME !!!"),
      Text("BTW ON VA DIRE QUE C'EST UNE PAGE DE CONNEXION PARCEQUE VOILà..."),
      Expanded(child: PassionApi()),
    ],),);
  }
}
